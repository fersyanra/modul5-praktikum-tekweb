<!DOCTYPE html>
<html>
<head>
	<title>Daftar Negara</title>
	<script src="jquery-3.4.1.min.js"></script>
	<script src="jquery.dataTables.min.js"></script>
	<link rel="stylesheet" href="jquery.dataTables.min.css">

</head>
<body>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#myTable').DataTable();
		});
	</script>

	<h1>Daftar Negara</h1>
	<table border="1" align="center" id="myTable" class="table table-striped table-bordered">
		<tr>
			<th>Code</th><th>Name</th>
		</tr>
		<?php
		foreach ($country->result() as $row) {
			echo "<tr>";
			echo "<td>$row->Code</td>";
			echo "<td>$row->Name</td>";
			echo "</tr>";
		}
		?>
	</table>
</body>
</html>