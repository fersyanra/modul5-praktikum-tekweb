<!DOCTYPE html>
<html>
<head>
	<title>Daftar Kota</title>
</head>
<body>
	<h1>Daftar Kota</h1>
	<?php
	$template = array(
		'table_open' => '<table border="1">'
	);
	$this->table->set_template($template);
	$this->table->set_heading("Nama","Negara","Populasi");
	foreach ($city->result() as $r) {
		$this->table->add_row($r->Name,$r->CountryCode,$r->Population);
	}
	echo $this->table->generate();
	?>
</body>
</html>