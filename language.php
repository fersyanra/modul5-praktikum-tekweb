<!DOCTYPE html>
<html>
<head>
	<title>Daftar Bahasa</title>
</head>
<body>
	<h1>Daftar Bahasa</h1>
	<?php
	$template = array(
		'table_open' => '<table border="1">'
	);
	$this->table->set_template($template);
	$this->table->set_heading("Kode Negara","Bahasa");
	foreach ($language->result() as $r) {
		$this->table->add_row($r->CountryCode,$r->Language);
	}
	echo $this->table->generate();
	?>

</body>
</html>